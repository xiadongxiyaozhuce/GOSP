# Emma
[这里保存着Emma的一部分劳动成果](https://gitee.com/k12edu/emma/)    

# 著作权
[这里所有的作品，其著作权都归Emma所有，](https://gitee.com/k12edu/emma/)    
未经著作权人允许，你不得将其作品用于牟利。    
除非你全部履行License文件里面规定的转载义务，否则你不能免费转载这里的作品。       

联系方式：692463260@qq.com    
具体的著作权声明、转载义务以这里的License文件内容为准  